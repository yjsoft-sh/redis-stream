module gitee.com/yjsoft-sh/redis-stream

go 1.20

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/satori/go.uuid v1.2.0
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.31.1 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)

package redis_stream

import "github.com/go-redis/redis"

type RedisStream struct {
	client *redis.Client
}

func New(client2 *redis.Client) (*RedisStream, error) {
	return &RedisStream{client: client2}, nil
}

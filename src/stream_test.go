package redis_stream

import (
	"fmt"
	"github.com/go-redis/redis"
	"testing"
	"time"
)

var stream *RedisStream

var queueName = "test_stream"

const delayName = "delay"

func init() {
	client := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})

	stream, _ = New(client)
}

func TestStream(t *testing.T) {
	stream.Add(queueName, "ok")
}

func TestRedisStream_Delay(t *testing.T) {
	stream.Delay(delayName, time.Now().Unix(), time.Minute)
}

func TestSubscribeDelay(t *testing.T) {
	group := stream.CreateConsumerGroup(delayName, &ConsumerOption{ConsumerCount: 2, IsDelay: true})
	group.Callback(func(message *Payload) error {
		fmt.Printf("%v\n", message)
		time.Sleep(time.Second * 10)
		return nil
	})
	if err := group.Listen(); err != nil {
		t.Fatal(err)
	}
}

func TestSubscribe(t *testing.T) {
	group := stream.CreateConsumerGroup(queueName, &ConsumerOption{ConsumerCount: 2})
	group.Callback(func(message *Payload) error {
		fmt.Printf("%v\n", message)
		return nil
	})
	if err := group.Listen(); err != nil {
		t.Fatal(err)
	}
}

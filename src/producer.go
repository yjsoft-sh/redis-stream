package redis_stream

import (
	"github.com/go-redis/redis"
	uuid "github.com/satori/go.uuid"
	"time"
)

func (r *RedisStream) Add(stream string, payload any) error {
	xaddArgs := redis.XAddArgs{
		Stream: stream,
		Values: map[string]interface{}{"payload": &Payload{Payload: payload}},
	}

	cmd := r.client.XAdd(&xaddArgs)

	return cmd.Err()
}

func (r *RedisStream) Delay(stream string, payload any, duration time.Duration) error {
	score := time.Now().Add(duration).UnixMicro()
	z := redis.Z{
		Score:  float64(score),
		Member: &Payload{Payload: payload, ID: uuid.NewV4().String()},
	}

	cmd := r.client.ZAdd(stream, z)
	return cmd.Err()
}
